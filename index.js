// import { getQuotes } from 'yf-api'
const { getQuotes } = require('yf-api');
const express = require("express");
const path = require("path");

// Calcule le MA 
const getMa = (nb, start, data) => {

    if (start < nb - 1) return 'nan';
    let ma;
    let sum = 0;
    for (let i = start; i > start - nb; i--) {
        // console.log(data[i]['close'],i, 'data close');

        sum += data[i]['close'];

    }
    console.log(sum, nb, 'data biiiig');

    ma = sum / nb;
    return ma;
}

const timeConverter = (UNIX_timestamp) => {
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
    return time;
}


var app = express();

// set the view engine to ejs
app.set('view engine', 'ejs');

// use res.render to load up an ejs view file

// index page
app.get('/', function (req, res) {
    try {

        getQuotes('XLM-USD', { interval: '15m', range: '8d' }).then((data) => {
            let lastGain = 0;
            let lastTop = 0;
            for (var i in data) {
                // parfois la derniere ou l'avant derniere valeur est nul
                if (!data[i]['close']) {
                    console.log(data[i]['close'], '//// DATA ERROR GETTING CAUSE TIMESTAMP');

                    data[i]['close'] = data[i - 1]['close'];
                }
                data[i]['timestamp'] = timeConverter(data[i]['timestamp']);
                data[i]['ma5'] = getMa(5, i, data);
                data[i]['ma20'] = getMa(20, i, data);
                data[i]['UP'] = data[i]['ma5'] > data[i]['ma20'] ? 'Buy' : 'Sell';
                data[i]['gain'] = data[i]['UP'] !== 'Buy'
                    ? data[lastGain]['close'] - data[i]['close']
                    : data[i]['close'] - data[lastGain]['close'];
                data[i]['benef'] = ((data[i]['gain'] / data[i]['close']) * 100).toFixed(2);
                data[i]['croiser'] = (data[i]['benef'] * 20).toFixed(2);

                if (parseInt(lastTop) < parseInt(data[i]['croiser'])) {
                    console.log('PAAAASS', lastTop, data[i]['croiser'], lastTop < data[i]['croiser'])
                    lastTop = data[i]['croiser'];
                    data[i]['top'] = true;
                    data[i-1]['top'] = null;
                } 

                if (i > 0 && data[i]['UP'] !== data[i - 1]['UP']) {
                    data[i]['CHANGE_POSITION'] = 'NOW';
                    data[i]['gain'] = data[i]['UP'] === 'Buy'
                        ? data[lastGain]['close'] - data[i]['close']
                        : data[i]['close'] - data[lastGain]['close'];
                    lastGain = i;
                    lastTop = 0;
                    data[i]['benef'] = ((data[i]['gain'] / data[i]['close']) * 100).toFixed(2);
                    data[i]['croiser'] = (data[i]['benef'] * 20).toFixed(2);
                }



            }
            data[data.length - 1]['gain'] = data[data.length - 1]['UP'] !== 'Buy'
                ? data[lastGain]['close'] - data[data.length - 1]['close']
                : data[data.length - 1]['close'] - data[lastGain]['close'];
            data[data.length - 1]['benef'] = ((data[data.length - 1]['gain'] / data[data.length - 1]['close']) * 100).toFixed(2);
            data[data.length - 1]['croiser'] = (data[data.length - 1]['benef'] * 20).toFixed(2);
            console.log(JSON.stringify(data[data.length - 1], null, 2), data[lastGain]);

            res.render('pages/index', { data });
        });
    } catch (e) {
        console.error(e);
    }
});

// about page
app.get('/about', function (req, res) {
    res.render('pages/about');
});

app.listen(8080);
console.log('Server is listening on port 8080');
